package Livraria;

public class Item {
	private String title;
	private String publisher;
	private int yearPublished;
	private String isbn;
	private double price;
	
	public Item(String title, String publisher, int yearPublished, String isbn, double price) {
		super();
		this.title = title;
		this.publisher = publisher;
		this.yearPublished = yearPublished;
		this.isbn = isbn;
		this.price = price;
	}

	public void display() {
		
		System.out.println("Titulo: "+this.title+"\nEditora: "+this.publisher+"\nData: "+this.yearPublished+"\nISBN: "+this.isbn+"\nPre�o: "+this.price);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getYearPublished() {
		return yearPublished;
	}
	public void setYaerPublished(int yearPublished) {
		this.yearPublished = yearPublished;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}
